msg = "trijam counter"
button = "start";
browser.browserAction.onClicked.addListener(function (tab) {
	// toggle
    switch (button) {
        case 'start':
            start();
            break;
        case 'stop':
            stop();
            break;
}
});
function start(){
	// 10800000 - 3h
	// 9900000 - 2:45
	// 5400000 - half time
	// 900000 - 15 mins
	// 60000 - minute
	button = 'stop';
	now = Date.now();
	//test = window.setTimeout(()=>{notif("test")},  120000);
	half = window.setTimeout(()=>{notif("half")},  5_400_000);
	before = window.setTimeout(()=>{notif("before")},  9_900_000);
	at = window.setTimeout(()=>{notif("at")},  10_800_000);
}
function stop(){
	now2 = Date.now();
	clearTimeout(half);
	clearTimeout(before);
	clearTimeout(at);
	// generate difference
	diff = now2-now;
	hours = diff/3_600_000; 
	minutes = (diff/60_000)-(Math.floor(hours)*60);
	seconds = (diff/1_000)-(Math.floor(minutes)*60);
	// copy time
	navigator.clipboard.writeText(
		/*diff+" full "+*/
		Math.floor(hours)+" hour/s ,"+
		Math.floor(minutes)+" minute/s and "+
		Math.floor(seconds)+" second/s"
	);
	button = 'start';
}
// notifications
function notif(which){
	switch (which){
		case "half":
			browser.notifications.create({
            type: 'basic',
            title: "it's half time to finish your job!",
            message: msg
            });
            break;
        case "before":
			browser.notifications.create({
            title: '15 MINUTES BEFORE DEADLINE!!!',
            type: "basic",
            message: msg
            });
            break;
        case "at":
			browser.notifications.create({
            type: 'basic',
            title: "TIME IS OUT!",
            message: msg
            });
            button = 'stop';
            navigator.clipboard.writeText("over time!");
            break;
        case "test":
			browser.notifications.create({
            type: 'basic',
            title: "test",
            message: msg
            });
            break;
	}
}
